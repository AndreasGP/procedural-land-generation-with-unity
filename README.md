# README #

This is a procedural land generator written for my Bachelor's thesis and computer graphics project course. It is written in C# with Unity. Currently the land generation is not multithreaded which means the application hangs when generating new land which could be fixed in a future version.

Computer graphics project course link: https://courses.cs.ut.ee/2016/cg-project/spring/Main/ProjectProceduralLandGeneration

## Used assets: ##

Terrain textures by Kenney - http://kenney.nl/

Tree models by Valday Team - https://www.assetstore.unity3d.com/en/#!/content/40444

## Usage: ##

You need the latest Unity installed to run the application without a precompiled build. 

### Controls: ###

C - toggle between camera and mouse cursor control.

F - to hide the sidebar.

N - to toggle flying.

Z - to descend while flying.

Space - to ascend while flying.

When cursor is given control, you can play around with the parameters in the sidebar.'

Render radius affects the number of chunks loaded at the same time. Large values can significally slow down the application.

Biome X and Y offset let you offset the noise used for determining biomes. For example setting X offset to 10, will shift all biomes by 10 blocks. This is for easily "fake-teleporting" around the world without actually having to move large distances.

Biome chunk width lets you change the size of each Voronoi cell that the terrain consists of.

Biome size frequency lets you control the size of biomes. Making it smaller than the default will make biomes larger and vice versa.

For the last 4 properties to take effect, the chunks must be regenerated using the given button.

This work is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License. See http://creativecommons.org/licenses/by-nc/4.0/ for more information.