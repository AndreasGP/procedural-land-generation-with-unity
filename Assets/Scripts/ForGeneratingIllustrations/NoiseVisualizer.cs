﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class NoiseVisualizer : MonoBehaviour {

    [SerializeField]
    private List<Image> layers;

    [SerializeField]
    private Image final;

    private int octaves = 6;

    private float frequency = 1 / 64f;

    private float lacunarity = 2f;

    private float persistence = 0.5f;

    void Start() {
        GenerateLayerTextures();
        GenerateFinalTexture();
    }

    private void GenerateLayerTextures() {

        var frequency = this.frequency;

        for(int layer = 0; layer < 6; layer++) {

            Debug.Log(frequency);

            var texture = new Texture2D(128, 128);
            texture.filterMode = FilterMode.Point;

            for (int x = 0; x < 128; x++) {
                for (int y = 0; y < 128; y++) {
                    
                    float noise = Noise.Value2D(new Vector3(x, y, 0), frequency).value * 0.5f + 0.5f;

                    texture.SetPixel(x, y, Col(noise));
                }
            }

            texture.Apply();
            layers[layer].material.mainTexture = texture;


            frequency *= lacunarity;
        }


    }

    private void GenerateFinalTexture() {

        var texture = new Texture2D(128, 128);
        texture.filterMode = FilterMode.Point;

        for (int x = 0; x < 128; x++) {
            for(int y = 0; y < 128; y++) {
                float noise = GetNoise(x, y);

                texture.SetPixel(x, y, Col(noise));
            }
        }

        texture.Apply();
        final.material.mainTexture = texture;
        
    }

    private float GetNoise(int x, int y) {
        return Noise.Sum(Noise.valueMethods[1], new Vector3(x, y, 0), frequency, octaves, lacunarity, persistence).value * 0.5f + 0.5f;
    }


    private Color Col(float greyscale) {
        return new Color(greyscale, greyscale, greyscale);
    }
}
