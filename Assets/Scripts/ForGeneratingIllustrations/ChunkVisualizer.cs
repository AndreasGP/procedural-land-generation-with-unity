﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ChunkVisualizer : MonoBehaviour {

    [SerializeField]
    private Material greenMaterial;

    [SerializeField]
    private Material redMaterial;

    void Start() {

        float gap = 1.2f;
        int width = 16;
        int height = 64;

        for (int x = 0; x < width; x++) {
            for (int z = 0; z < width; z++) {
                for (int y = 0; y < height; y++) {
                    var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    cube.transform.position = new Vector3((x - width/2) * gap, (y - height/2) * gap, (z - width/2) * gap);
                    cube.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);

                    if(x == width - 1 && z == width - 2) {
                        cube.GetComponent<Renderer>().material = redMaterial;
                        if(y == height - 2) {
                            cube.GetComponent<Renderer>().material = greenMaterial;
                        }
                    } else {
                        cube.GetComponent<Renderer>().material.color = new Color(0.2f, 0.5f, 1f);
                    }

                }
            }
        }
    }


}
