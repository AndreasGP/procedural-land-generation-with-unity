﻿using UnityEngine;
using System.Collections;

public class TreeNoiseTester : MonoBehaviour {

    void Start() {
        GeneratePositions();
    }

    void Update() {

    }

    private void GeneratePositions() {

        for (int x = 0; x < 100; x++) {
            for (int y = 0; y < 100; y++) {
                if (SuitablePosition(x, y)) {
                    CreateCube(x, y);
                }
            }
        }
    }

    private bool SuitablePosition(int x, int y) {

        var noiseXY = GetNoise(x, y);

        for (int _x = x - 1; _x <= x + 1; _x++) {
            for (int _y = y - 1; _y <= y + 1; _y++) {
                if (_x == x && _y == y) continue;

                if(GetNoise(_x, _y) >= noiseXY) {
                    return false;
                }
            }
        }

        return true;
    }


    private float GetNoise(int x, int y) {
        return Noise.Sum(Noise.simplexMethods[1], new Vector3(x, y, 0), 0.16f, 1, 2, 0.5f).value * 0.5f + 0.5f;
    }


    private void CreateCube(int x, int y) {
        var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.GetComponent<Renderer>().material.color = new Color(0, 0, 1);
        cube.transform.position = new Vector3(x, 0, y);
    }
}
