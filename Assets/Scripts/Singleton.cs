﻿using UnityEngine;

namespace ProceduralLandGeneration {
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour {

        private static T instance;

        public static T ins {
            get {
                if (instance == null) {
                    instance = (T)FindObjectOfType(typeof(T));
                }

                return instance;
            }
        }
    }
}
