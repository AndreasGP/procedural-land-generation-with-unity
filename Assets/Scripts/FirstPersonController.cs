﻿using UnityEngine;

namespace ProceduralLandGeneration {
    [RequireComponent(typeof(CharacterController))]
    public class FirstPersonController : MonoBehaviour {

        [SerializeField]
        private float runSpeed;

        [SerializeField]
        private float sprintMultiplier;

        [SerializeField]
        private float walkMultiplier;

        [SerializeField]
        private float jumpHeight;

        [SerializeField]
        private float gravityMultiplier;

        [SerializeField]
        private Camera camera;

        private float cameraSensitivity = 140f;
        private float cameraRotationX = 0.0f;
        private float cameraRotationY = 0.0f;

        private CharacterController characterController;

        private bool pressedJump = false;

        private bool isJumping = false;

        private bool wasOnGround = false;

        private bool fixed_wasOnGround = false;

        private Vector3 moveDirection;

        private Vector3 momentum;

        private bool cameraOn = true;

        private bool noclipOn = false;

        private Vector3 oldPosition;

        private int positionFrameSkipCounter = 0;

        private bool justStoppedMoving = false;

        private Quaternion oldRotation;

        private int rotationFrameSkipCounter = 0;

        private bool justStoppedRotating = false;


        public delegate void OnPlayerMovedHandler(Vector3 newPosition);
        public event OnPlayerMovedHandler OnPlayerMoved;

        public delegate void OnPlayerRotatedHandler(Quaternion newRotation);
        public event OnPlayerRotatedHandler OnPlayerRotated;

        void Start() {
            characterController = GetComponent<CharacterController>();

            moveDirection = new Vector3();
            momentum = new Vector3();

            SetSensitivity(PlayerPrefs.GetInt("mousesensitivity", 15));
        }

        void Update() {

            if(Input.GetKeyDown(KeyCode.N)) {
                noclipOn = !noclipOn;
            }

            if(Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.C)) {
                cameraOn = !cameraOn;
            }

            if (!cameraOn) {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                return;
            }

            if (cameraOn) {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            } else {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }


            if (Input.GetMouseButton(2) || cameraOn) {
                cameraRotationX += Input.GetAxis("Mouse X") * cameraSensitivity * Time.deltaTime;
                cameraRotationY += Input.GetAxis("Mouse Y") * cameraSensitivity * Time.deltaTime;
                cameraRotationY = Mathf.Clamp(cameraRotationY, -89.999f, 89.999f);


                camera.transform.localRotation = Quaternion.AngleAxis(cameraRotationX, Vector3.up);
                camera.transform.localRotation *= Quaternion.AngleAxis(cameraRotationY, Vector3.left);
            }


            if (pressedJump == false) {
                pressedJump = Input.GetKeyDown(KeyCode.Space);
            }

            if (characterController.isGrounded) {

                if (wasOnGround == false) {
                    //Landed 
                    isJumping = false;
                    moveDirection.y = 0;

                }

            }

            wasOnGround = characterController.isGrounded;
        }

        internal void SetSensitivity(int sensitivity) {
            cameraSensitivity = sensitivity * 10f;
        }

        void FixedUpdate() {

            //Decrease momentum
            momentum.Scale(new Vector3(0.97f, 0.97f, 0.97f));

            if (noclipOn) {
                moveDirection.y = 0;
            }

            float forwardSpeed = 0;
            float sidewaysSpeed = 0;

            if (cameraOn) {
                if (Input.GetKey(KeyCode.W)) {
                    forwardSpeed = 1f;
                }
                if (Input.GetKey(KeyCode.S)) {
                    forwardSpeed = -1f;
                }

                if (Input.GetKey(KeyCode.D)) {
                    sidewaysSpeed = 1f;
                }
                if (Input.GetKey(KeyCode.A)) {
                    sidewaysSpeed = -1f;
                }

                if (noclipOn) {
                    if (Input.GetKey(KeyCode.Space)) {
                        moveDirection.y += 25;
                    }
                    if (Input.GetKey(KeyCode.Z)) {
                        moveDirection.y -= 25;
                    }
                }
            }

            //Apply gravity
            if (noclipOn == false && !characterController.isGrounded) {
                //Don't apply gravity for first x seconds of the game.
                if (Time.realtimeSinceStartup > 1) {
                    moveDirection += Physics.gravity * Time.fixedDeltaTime * gravityMultiplier;
                }
            }

            if (transform.position.y < -10) {
                transform.position = new Vector3(transform.position.x, 100, transform.position.z);
                moveDirection = new Vector3();
            }

            // always move along the camera forward as it is the direction that it being aimed at
            Vector3 desiredMove = camera.transform.forward * forwardSpeed + camera.transform.right * sidewaysSpeed;

            // get a normal for the surface that is being touched to move along it
            RaycastHit hitInfo;
            //Physics.SphereCast(transform.position, characterController.radius, Vector3.down, out hitInfo, characterController.height / 2f);
            //desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;
            desiredMove.y = 0;
            desiredMove = desiredMove.normalized;

            float realSpeed = runSpeed;

            if (Input.GetKey(KeyCode.LeftShift)) {
                realSpeed *= sprintMultiplier;
            }

            if (Input.GetKey(KeyCode.LeftControl)) {
                realSpeed *= walkMultiplier;
            }


            moveDirection.x = desiredMove.x * realSpeed;
            moveDirection.z = desiredMove.z * realSpeed;



            if (characterController.isGrounded) {
                moveDirection.y = -10f;//sticktogroundforce?

                if (pressedJump) {
                    moveDirection.y = jumpHeight;
                    pressedJump = false;
                    isJumping = true;
                }
            }

            characterController.Move((moveDirection) * Time.fixedDeltaTime);


            //if (fixed_wasOnGround == false && characterController.isGrounded == true) {
            //    Debug.Log("Increasing momentum from " + momentum);
            //    Vector3 increasedMomentum = new Vector3(2f, 0.05f, 2f);
            //    Debug.Log("Move dir is " + moveDirection);
            //    increasedMomentum.Scale(moveDirection);

            //    float increase = 0.01f;
            //    momentum.x += momentum.x >= 0 ? increase : - increase;
            //    momentum.z += momentum.x >= 0 ? increase : -increase;

            //    momentum.Scale(increasedMomentum);
            //    Debug.Log("Is now " + momentum);
            //}

            //Debug.Log("Momentum: " + momentum);


            //fixed_wasOnGround = characterController.isGrounded;

            //Sending position updates to the server
            if (oldPosition != transform.position) {
                positionFrameSkipCounter++;

                //TODO: TEMPORARY
                if (positionFrameSkipCounter == 7) {
                    positionFrameSkipCounter = 0;
                    if (OnPlayerMoved != null) {
                        OnPlayerMoved(transform.position);
                    }
                }
                justStoppedMoving = true;
            } else {
                if (justStoppedMoving) {
                    justStoppedMoving = false;
                    if (OnPlayerMoved != null) {
                        OnPlayerMoved(transform.position);
                    }
                }
            }
            oldPosition = transform.position;

            //Sending rotation updates to the server
            if (oldRotation != camera.transform.rotation) {
                rotationFrameSkipCounter++;

                //TODO: TEMPORARY
                if (rotationFrameSkipCounter == 7) {
                    rotationFrameSkipCounter = 0;
                    if (OnPlayerRotated != null) {
                        OnPlayerRotated(camera.transform.rotation);
                    }
                }
                justStoppedRotating = true;
            } else {
                if (justStoppedRotating) {
                    justStoppedRotating = false;
                    if (OnPlayerRotated != null) {
                        OnPlayerRotated(camera.transform.rotation);
                    }
                }
            }
            oldRotation = camera.transform.rotation;
        }
    }
}
