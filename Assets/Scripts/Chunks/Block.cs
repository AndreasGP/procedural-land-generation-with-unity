﻿using UnityEngine;
using System.Collections;

namespace ProceduralLandGeneration.Chunks {
    //Contains the IDs of all the possible blocks.
    public static class Block {
        public const short EMPTY = 0;
        public const short GRASS_NORMAL = 1;
        public const short DIRT_NORMAL = 2;
        public const short GRASS_DRY = 3;
        public const short DIRT_DRY = 4;
        public const short GRASS_HOT = 5;
        public const short DIRT_HOT = 6;
        public const short GRASS_FOREST = 7;
        public const short DIRT_FOREST = 8;
        public const short SAND = 9;
        public const short SNOW = 10;
        public const short WATER = 11;
        public const short STONE = 12;

    }
}
