﻿using UnityEngine;
using System.Collections;
using ProceduralLandGeneration.Util;

namespace ProceduralLandGeneration.Chunks {

    public class ChunkData {

        //Global chunk coordinates of the given chunk data.
        private Point2 chunkPos;

        /// <summary>
        /// An array consisting of data values for chunk voxels.
        /// While the array contains w * h * l values, a 1D array is used for speed benefits.
        /// To translate between 1D and 3D array coordinates, see CoordUtil class.
        /// </summary>
        private short[] data;

        public ChunkData(Point2 chunkPos) {
            this.chunkPos = chunkPos;

            data = new short[ChunkConstants.chunkvolume];
        }

        public short GetBlock(Point3 localPos) {
            return GetBlock(localPos.x, localPos.y, localPos.z);
        }

        public short GetBlock(int x, int y, int z) {
            int index = CoordUtil.xyzToI(x, y, z);
            return GetBlock(index);
        }

        public short GetBlock(int index) {
            return data[index];
        }

        public void SetBlock(Point3 localPos, short id) {
            SetBlock(localPos.x, localPos.y, localPos.z, id);
        }

        public void SetBlock(int x, int y, int z, short id) {
            int index = CoordUtil.xyzToI(x, y, z);
            SetBlock(index, id);
        }

        public void SetBlock(int index, short id) {
            data[index] = id;
        }

        public bool IsEmpty(Point3 localPos) {
            return IsEmpty(localPos.x, localPos.y, localPos.z);
        }

        public bool IsEmpty(int x, int y, int z) {
            return IsEmpty(CoordUtil.xyzToI(x, y, z));
        }

        public bool IsEmpty(int index) {
            return data[index] == Block.EMPTY;
        }

    }
}
