﻿using UnityEngine;
using System.Collections;
using ProceduralLandGeneration.Util;
using System.Collections.Generic;
using System.Linq;

namespace ProceduralLandGeneration.Chunks {

    /// <summary>
    /// A chunk generator used for generating chunks' chunkdata
    /// </summary>
    public class ChunkGenerator {
        
        public ChunkData GenerateChunkData(Chunk chunk, Point2 chunkPos) {
            //Debug.Log("=================================");
            //Debug.Log("CHUNK " + chunkPos.x + " " + chunkPos.z);

            var data = new ChunkData(chunkPos);

            //Calculate a biome lookup table for this chunk
            //The key is the biomeblock coordinates, value is the biome instance
            Dictionary<Point2, Biome> biomes = GenerateChunkBiomeTable(chunkPos);

            for (int x = 0; x < ChunkConstants.chunkwidth; x++) {
                for (int z = 0; z < ChunkConstants.chunkwidth; z++) {
                    //Iterating over chunk columns

                    var globalBlock = CoordUtil.LocalBlockToGlobalBlock(chunkPos.x, chunkPos.z, x, 0, z);

                    //Find a list of all the closest biomes sorted by distance. First arg is the biome,
                    //second arg is the distance to the current column.
                    List<Pair<Biome, float>> closestBiomes = GetClosestBiomes(biomes, globalBlock);

                    //Assign the closest biome center biome to this column
                    var biome = closestBiomes[0].x;

                    short block = biome.block;

                    //Calculate the new amplification depending on this columns distance to the closest other biome
                    float amplification = CalculateAmplification(closestBiomes);

                    //Calculate a noise value that is used for the heightmap calculations.
                    var noise2D = Noise.Sum(Noise.simplexMethods[1], new Vector3(globalBlock.x, globalBlock.z, 0), 0.005f, 3, 2f, 0.5f).value * 0.5f + 0.5f;

                    int height = (int)(noise2D * ChunkConstants.chunkheight * amplification);

                    //Make sure the ground is always at least 1 point high so there are no gaps to fall through in the terrain.
                    if(height == 0) {
                        height = 1;
                    }

                    bool isGrass = true;

                    //Start iterating down the column from the top
                    for (int y = ChunkConstants.chunkheight - 1; y >= 0; y--) {
                        //If the biome has grass, choose if the block should be grass or dirt.
                        if (block == Block.GRASS_NORMAL) {
                            block = isGrass ? Block.GRASS_NORMAL : Block.DIRT_NORMAL;
                        } else if (block == Block.GRASS_DRY) {
                            block = isGrass ? Block.GRASS_DRY : Block.DIRT_DRY;
                        } else if (block == Block.GRASS_HOT) {
                            block = isGrass ? Block.GRASS_HOT : Block.DIRT_HOT;
                        } else if (block == Block.GRASS_FOREST) {
                            block = isGrass ? Block.GRASS_FOREST : Block.DIRT_FOREST;
                        }


                        if (y < height) {
                            //current value is below the heightmap cutoff, therefore...
                            data.SetBlock(x, y, z, block);

                            if(isGrass && biome.treeGenerationFrequency != 0f) {
                                //At top of a block, might be possible to generate a tree here
                                if(IsValidTreePosition(globalBlock.x, globalBlock.z, biome.treeGenerationFrequency)) {
                                    //Place a tree above this block
                                    var tree = GetTree(globalBlock.x, globalBlock.z, block);

                                    if(tree != null) {
                                        tree.transform.position = new Vector3(
                                            (x + 0.5f) * ChunkConstants.blockwidth,
                                            (y + 1f) * ChunkConstants.blockheight,
                                            (z + 0.5f) * ChunkConstants.blockwidth);
                                        tree.transform.SetParent(chunk.transform);
                                    }
                                }
                            }
                            isGrass = false;
                        } else {
                            //TODO: could implement using the 3D noise here
                            isGrass = true;
                        }
                    }
                }
            }

            return data;
        }

        //Returns the tree object for the given biome and global column coordinates.
        private GameObject GetTree(int x, int y, short biomeBlock) {

            var random = new System.Random((int)(Mathf.Sin(x * 31 + y) * 1372) * y * 31);

            GameObject prefab = null;

            if(biomeBlock == Block.GRASS_HOT) {
                //Savanna
                var count = ChunkManager.ins.savannaTreePrefabs.Count;
                prefab = ChunkManager.ins.savannaTreePrefabs[random.Next() % count];

            } else if(biomeBlock == Block.GRASS_FOREST) {
                //Forest
                var count = ChunkManager.ins.forestTreePrefabs.Count;
                prefab = ChunkManager.ins.forestTreePrefabs[random.Next() % count];

            } else if(biomeBlock == Block.GRASS_NORMAL) {
                //Grassland
                var count = ChunkManager.ins.grasslandTreePrefabs.Count;
                prefab = ChunkManager.ins.grasslandTreePrefabs[random.Next() % count];

            } else if (biomeBlock == Block.SAND) {
                //Warm desert
                var count = ChunkManager.ins.desertTreePrefabs.Count;
                prefab = ChunkManager.ins.desertTreePrefabs[random.Next() % count];

            } else if (biomeBlock == Block.SNOW) {
                //Cold desert
                var count = ChunkManager.ins.snowTreePrefabs.Count;
                prefab = ChunkManager.ins.snowTreePrefabs[random.Next() % count];

            }

            if (prefab != null) {
                var instance = GameObject.Instantiate(prefab);
                instance.transform.Rotate(Vector3.up, random.Next() / 360);
                if (prefab.name == "Grass_2_pal") {
                    instance.transform.localScale = new Vector3(12f, 12f, 12f);
                } else if (prefab.name == "Tree_01_summer_pal") {
                    instance.transform.localScale = new Vector3(5f, 5f, 5f);
                } else {
                    instance.transform.localScale = new Vector3(3f, 3f, 3f);
                }
                return instance;

            }

            return null;

        }

        private bool IsValidTreePosition(int x, int y, float treeGenerationFrequency) {
            var noiseXY = GetTreePositionNoise(x, y, treeGenerationFrequency);

            //Check if the noise value at the given point is the "local maximum".
            for (int _x = x - 1; _x <= x + 1; _x++) {
                for (int _y = y - 1; _y <= y + 1; _y++) {
                    if (_x == x && _y == y) continue;
                    if (GetTreePositionNoise(_x, _y, treeGenerationFrequency) >= noiseXY) {
                        return false;
                    }
                }
            }
            return true;
        }

        //Returns the noise value at given global column coordinates with the given plant generation frequency.
        private float GetTreePositionNoise(int globalX, int globalZ, float frequency) {
            return Noise.Sum(Noise.simplexMethods[1], new Vector3(globalX, globalZ, 0), frequency, 1, 2, 0.5f).value * 0.5f + 0.5f;
        }

        //Calculates the new amplification depending on the closest biomes.
        //If the second closest biome is far away enough, then just closest
        //biome amplification is used.
        private float CalculateAmplification(List<Pair<Biome, float>> closestBiomes) {

            var closestBiome = closestBiomes[0].x;
            var closestDist = closestBiomes[0].y;

            //The denominator of x_i average and x_weighted_i calculations.
            var ampWeightsSum = closestBiome.blendFactor;

            //Denoted as x_i average or x_weighted_i in the thesis.
            var lerpMax = closestBiome.blendFactor * closestBiome.amplification;
            
            //The final output amplification, denoted as v in the thesis.
            var amp = closestBiome.amplification;

            for (int i = 1; i < closestBiomes.Count; i++) {
                //For each close biome that is not the one the column is on...
                var otherBiome = closestBiomes[i].x;
                var otherDist = closestBiomes[i].y;

                //Denoted as |d_i - d| in the thesis.
                var delta = Mathf.Abs(closestDist - otherDist);

                float w = ChunkConstants.biomewidth * 0.8f;

                if(delta < w) {

                    var oldAmpWeightsSum = ampWeightsSum;

                    ampWeightsSum += otherBiome.blendFactor;

                    //Increase lerpMax so it factors in the new biome's blend and amplification
                    lerpMax = lerpMax * oldAmpWeightsSum / (ampWeightsSum) + otherBiome.blendFactor * otherBiome.amplification / (ampWeightsSum);
                    amp = Mathf.Lerp(lerpMax, amp, delta / w);
                }
            }

            return amp;
        }

        //Returns a list with the closest  biomes ordered and distances to them, first one is the closest.
        private List<Pair<Biome, float>> GetClosestBiomes(Dictionary<Point2, Biome> biomes, Point3 globalBlock) {
            List<Pair<Biome, float>> closestBiomes = new List<Pair<Biome, float>>();

            //Using the joys of C#'s LINQ to sort the closest biomes.
            List<Biome> biomesOrderedByDistance = biomes.Values.GroupBy(x => x.block).
                Select(y => y.OrderBy(z => z.GetDistanceSquare(globalBlock.x, globalBlock.z)).First()).
                OrderBy(w => w.GetDistanceSquare(globalBlock.x, globalBlock.z)).ToList();


            foreach(var biome in biomesOrderedByDistance) {
                closestBiomes.Add(new Pair<Biome, float>(biome, Mathf.Sqrt(biome.GetDistanceSquare(globalBlock.x, globalBlock.z))));
            }
            
            return closestBiomes;
        }


        /// <summary>
        /// Generates a dictionary with keys consisting of biome block coordinates and biome values
        /// </summary>
        /// <param name="chunkPos"></param>
        /// <returns></returns>
        private Dictionary<Point2, Biome> GenerateChunkBiomeTable(Point2 chunkPos) {
            var biomes = new Dictionary<Point2, Biome>();

            var width = ChunkConstants.chunkwidth;

            //Calculate the minimum and maximum biomeblocks that are of interest for this chunk
            var minGlobalBlock = CoordUtil.LocalBlockToGlobalBlock(chunkPos, new Point3(0, 0, 0));
            var minBiomeBlock = CoordUtil.GlobalBlockToBiomeBlock(minGlobalBlock);
            var maxGlobalBlock = CoordUtil.LocalBlockToGlobalBlock(chunkPos, new Point3(width - 1, 0, width - 1));
            var maxBiomeBlock = CoordUtil.GlobalBlockToBiomeBlock(maxGlobalBlock);

            for(int x = minBiomeBlock.x - 2; x <= maxBiomeBlock.x + 2; x++) {
                for (int z = minBiomeBlock.z - 2; z <= maxBiomeBlock.z + 2; z++) {
                    var biome = GetBiomeAtBiomeBlock(x, z);
                    biomes.Add(new Point2(x, z), biome);
                }
            }
            return biomes;
        }

        private Biome GetBiomeAtBiomeBlock(int biomeBlockX, int biomeBlockZ) {
            //Calculate the local x and y offsets for the biome midpoint
            var xNoise = Noise.Sum(Noise.valueMethods[1], new Vector3(biomeBlockX + 1000, biomeBlockZ), 1f, 1, 2, 0.5f).value * 0.5f + 0.5f;
            var xOffset = (int)(xNoise * ChunkConstants.biomewidth);

            var yNoise = Noise.Sum(Noise.valueMethods[1], new Vector3(biomeBlockX, biomeBlockZ + 1000), 1f, 1, 2, 0.5f).value * 0.5f + 0.5f;
            var zOffset = (int)(yNoise * ChunkConstants.biomewidth);

            Biome biome = new Biome(biomeBlockX, biomeBlockZ, xOffset, zOffset);
            return biome;
        }

    }
}
