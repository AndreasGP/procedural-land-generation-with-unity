﻿using UnityEngine;
using System.Collections;
using ProceduralLandGeneration.Util;

namespace ProceduralLandGeneration.Chunks {

    //One piece of the world
    public class Chunk : MonoBehaviour {

        //The global coordinates of the block
        private Point2 position;

        //The external data of the chunk.
        private ChunkData chunkData;

        public void Initialize(Point2 position) {
            this.position = position;
            name = "Chunk " + position.x + " " + position.z;

            //Step 1: Generate the data for the chunk
            LoadChunkData();

            //Step 2: Build a mesh based on the data
            GenerateChunkMesh();
        }

        private void LoadChunkData() {
            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            chunkData = ChunkManager.ins.chunkGenerator.GenerateChunkData(this, position);

            sw.Stop();
            //Debug.Log("Took " + sw.ElapsedMilliseconds + " ms to generate chunk data.");
        }

        private void GenerateChunkMesh() {
            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            var mesh = ChunkManager.ins.chunkMeshBuilder.BuildMesh(chunkData);

            GetComponent<MeshFilter>().sharedMesh = mesh;
            GetComponent<MeshCollider>().sharedMesh = mesh;

            sw.Stop();
            //Debug.Log("Took " + sw.ElapsedMilliseconds + " ms to generate chunk mesh.");
        }

    }
}
