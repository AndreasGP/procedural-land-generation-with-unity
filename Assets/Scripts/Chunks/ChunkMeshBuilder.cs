﻿using UnityEngine;
using System.Collections;
using ProceduralLandGeneration.Util;
using System;

namespace ProceduralLandGeneration.Chunks {

    public class ChunkMeshBuilder {

        /// <summary>
        /// Builds the mesh corresponding to the given chunk data.
        /// </summary>
        /// <param name="chunkData"></param>
        /// <returns></returns>
        public Mesh BuildMesh(ChunkData chunkData) {
            var meshBuilder = new MeshBuilder();

            for (int x = 0; x < ChunkConstants.chunkwidth; x++) {
                for (int z = 0; z < ChunkConstants.chunkwidth; z++) {
                    for (int y = 0; y < ChunkConstants.chunkheight; y++) {
                        BuildBlock(meshBuilder, chunkData, x, y, z);
                    }
                }
            }

            return meshBuilder.ToMesh();
        }

        private void BuildBlock(MeshBuilder meshBuilder, ChunkData chunkData, int x, int y, int z) {
            short block = chunkData.GetBlock(x, y, z);

            if (block == Block.EMPTY) {
                return;
            }

            foreach (CubeSide cubeSide in Enum.GetValues(typeof(CubeSide))) {
                if(IsSideVisible(chunkData, x, y, z, cubeSide)) {
                    GenerateQuad(meshBuilder, x, y, z, block, cubeSide);
                }
            }

        }

        private bool IsSideVisible(ChunkData chunkData, int x, int y, int z, CubeSide cubeSide) {
            Point3 originalPos = new Point3(x, y, z);

            Point3 otherPos = cubeSide.ToPoint();
            otherPos += originalPos;
            //otherPos now contains the block coordinates that is 1 block in the given cubeside direction.

            if (otherPos.y >= ChunkConstants.chunkheight) {
                //Always draw the top of the chunks
                return true;
            }
            if (otherPos.y < 0) {
                //Never draw the bottom of the chunks
                return false;
            }

            if(CoordUtil.IsOnLocalChunk(otherPos)) {
                if(chunkData.IsEmpty(otherPos)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                //should check neighbouring chunk, atm just returning true
                return true;
            }
        }

        private void GenerateQuad(MeshBuilder meshBuilder, int x, int y, int z, short block, CubeSide cubeSide) {
            int[] corners = MeshTable.squareTable[cubeSide];
            meshBuilder.AddQuadIndices(0);

            Vector3[] vertices = new Vector3[corners.Length];
            for (int i = 0; i < corners.Length; i++) {
                vertices[i] = VertexUtil.ToVertexPosition(corners[i]);
            }

            Vector3 verticesOffset = new Vector3(x * (ChunkConstants.blockwidth), y * (ChunkConstants.blockheight), z * (ChunkConstants.blockwidth));
            meshBuilder.AddVertices(vertices, verticesOffset);

            Vector3 normal = MeshTable.normalTable[(int)cubeSide];
            Vector3[] normals = new Vector3[4];
            for (int i = 0; i < normals.Length; i++) {
                normals[i] = normal;
            }
            meshBuilder.AddNormals(normals);

            var texCoords = GetTexCoords(block, cubeSide);
            meshBuilder.AddTexCoords(texCoords);

            Color[] colors = new Color[corners.Length];
            for (int i = 0; i < colors.Length; i++) {
                colors[i] = new Color(0, 1, 0);
                meshBuilder.AddColor(colors[i]);
            }
        }

        private Vector2[] GetTexCoords(short block, CubeSide side) {

            if (block == Block.GRASS_NORMAL) {
                if (side == CubeSide.YM) {
                    return TranslateCoords(1, 0);
                } else if (side == CubeSide.Ym) {
                    return TranslateCoords(0, 0);
                } else {
                    return TranslateCoords(2, 0);
                }
            } else if (block == Block.DIRT_NORMAL) {
                return TranslateCoords(0, 0);
            } else if (block == Block.GRASS_DRY) {
                if (side == CubeSide.YM) {
                    return TranslateCoords(1, 1);
                } else if (side == CubeSide.Ym) {
                    return TranslateCoords(0, 1);
                } else {
                    return TranslateCoords(2, 1);
                }
            } else if (block == Block.DIRT_DRY) {
                return TranslateCoords(0, 1);
            } else if (block == Block.GRASS_HOT) {
                if (side == CubeSide.YM) {
                    return TranslateCoords(0, 2);
                } else if (side == CubeSide.Ym) {
                    return TranslateCoords(1, 3);
                } else {
                    return TranslateCoords(1, 2);
                }
            } else if (block == Block.DIRT_HOT) {
                return TranslateCoords(1, 3);
            } else if (block == Block.GRASS_FOREST) {
                if (side == CubeSide.YM) {
                    return TranslateCoords(1, 0);
                } else if (side == CubeSide.Ym) {
                    return TranslateCoords(0, 0);
                } else {
                    return TranslateCoords(2, 0);
                }
            } else if (block == Block.DIRT_FOREST) {
                return TranslateCoords(0, 0);
            } else if (block == Block.SAND) {
                return TranslateCoords(3, 2);
            } else if (block == Block.SNOW) {
                return TranslateCoords(2, 2);
            } else if (block == Block.WATER) {
                return TranslateCoords(3, 3);
            } else if (block == Block.STONE) {
                return TranslateCoords(3, 0);
            }


            return TranslateCoords(3, 1);
        }


        private Vector2[] TranslateCoords(int texX, int texY) {

            Vector2[] coords = new Vector2[4];

            var offset = 0.04166666f;
            var texWidth = 0.166666666f;

            texY = 3 - (texY);

            int i = 0;
            for (int y = 0; y <= 1; y++) {
                for (int x = 0; x <= 1; x++) {
                    coords[i++] = new Vector2(texX / 4f + offset + x * texWidth, texY / 4f + offset + y * texWidth);
                }
            }

            return coords;
        }

    }

}
