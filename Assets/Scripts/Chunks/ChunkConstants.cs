﻿using UnityEngine;
using System.Collections;
using System;

namespace ProceduralLandGeneration.Chunks {

    public static class ChunkConstants {

        //Should be a power of 2 or this implementation breaks.
        public static readonly int chunkwidth = 16;

        //Should be a power of 2 or this implementation breaks.
        public static readonly int chunkheight = 64;

        /// <summary>
        /// Width of one biome chunk.
        /// </summary>
        public static int biomewidth = 12;

        /// <summary>
        /// The width of 1 block in world space
        /// </summary>
        public static readonly float blockwidth = 2f;

        /// <summary>
        /// The height of 1 block in world space
        /// </summary>
        public static readonly float blockheight = 2f;

        /// <summary>
        /// Area of one horizontal slize of the chunk. Do not touch.
        /// </summary>
        public static readonly int chunkarea = chunkwidth * chunkwidth;

        /// <summary>
        /// Volume of a chunk. Do not touch.
        /// </summary>
        public static readonly int chunkvolume = chunkwidth * chunkheight * chunkwidth;

        /// <summary>
        /// The base 2 logarithm of chunkwidth. For example 4 is 2, 8 is 3, 16 is 4 etc. Do not touch.
        /// </summary>
        public static readonly int chunkwidthbit = Convert.ToString(chunkwidth - 1, 2).Length;
    }
}
