﻿using ProceduralLandGeneration.Util;
using System.Collections.Generic;
using UnityEngine;

namespace ProceduralLandGeneration.Chunks {

    public class ChunkManager : Singleton<ChunkManager> {

        #region serialized fields
        [SerializeField]
        [Tooltip("Whether or not to update the loaded chunks when the player moves.")]
        public bool autoGenerate = true;

        [SerializeField]
        [Tooltip("The radius in chunks around the user indicating how many chunks to generate.")]
        public int generationRadius = 6;

        [SerializeField]
        private Chunk chunkPrefab;

        //lists of possible trees in each biome.
        [SerializeField]
        public List<GameObject> forestTreePrefabs;

        [SerializeField]
        public List<GameObject> savannaTreePrefabs;

        [SerializeField]
        public List<GameObject> grasslandTreePrefabs;

        [SerializeField]
        public List<GameObject> snowTreePrefabs;

        [SerializeField]
        public List<GameObject> desertTreePrefabs;
        #endregion

        private Dictionary<Point2, Chunk> chunks;

        public ChunkGenerator chunkGenerator { get; private set; }
        public ChunkMeshBuilder chunkMeshBuilder { get; private set; }

        void Start() {
            chunks = new Dictionary<Point2, Chunk>();

            chunkGenerator = new ChunkGenerator();
            chunkMeshBuilder = new ChunkMeshBuilder();

            if (autoGenerate == false) {
                //If auto generation is turned off, still generate some chunks around the player in the beginning.
                int radius = generationRadius;
                for (int x = -radius; x <= radius; x++) {
                    for (int y = -radius; y <= radius; y++) {
                        LoadChunk(new Point2(x, y));
                    }
                }
            }
        }

        void Update() {
            if(autoGenerate) {
                UpdateSurroundingChunks();
            }
        }

        public void UnloadAll() {
            foreach(var chunk in chunks.Values) {
                Destroy(chunk.gameObject);
            }
            chunks.Clear();
        }

        private void UpdateSurroundingChunks() {
            var playerChunk = CoordUtil.GlobalBlockToChunk(CoordUtil.GlobalPositionToGlobalBlock(Camera.main.transform.position));

            //Load any missing chunks
            int toBeLoaded = 0;
            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            for (int x = -generationRadius; x <= generationRadius; x++) {
                for(int y = -generationRadius; y <= generationRadius; y++) {
                    var chunkPos = new Point2(x + playerChunk.x, y + playerChunk.z);

                    if(chunks.ContainsKey(chunkPos) == false) {
                        LoadChunk(chunkPos);
                        toBeLoaded++;
                    }
                }
            }

            sw.Stop();
            //Debug.Log("Took " + sw.ElapsedMilliseconds + " ms to generate  "  + toBeLoaded + " chunks.");

            //Remove any obsolete chunks
            List<Point2> markedForRemoval = new List<Point2>();
            foreach (var entry in chunks) {
                var chunkPos = entry.Key;
                if(Mathf.Abs(playerChunk.x - chunkPos.x) > generationRadius || 
                    Mathf.Abs(playerChunk.z - chunkPos.z) > generationRadius) {
                    markedForRemoval.Add(chunkPos);
                    UnloadChunk(chunkPos);
                }
            }

            foreach(var chunkPos in markedForRemoval) {
                chunks.Remove(chunkPos);
            }

        }

        public void LoadChunk(Point2 chunkPos) {
            if (chunks.ContainsKey(chunkPos) == false) {
                var chunk = Instantiate(chunkPrefab);
                chunk.Initialize(chunkPos);
                float posX = chunkPos.x * (ChunkConstants.chunkwidth + 0.0f) * ChunkConstants.blockwidth;
                float posZ = chunkPos.z * (ChunkConstants.chunkwidth + 0.0f) * ChunkConstants.blockwidth;
                chunk.transform.position = new Vector3(posX, 0, posZ);
                chunks.Add(chunkPos, chunk);
            }
        }

        public void UnloadChunk(Point2 chunkPos) {
            if(chunks.ContainsKey(chunkPos)) {
                var chunk = chunks[chunkPos];
                Destroy(chunk.gameObject);
            }
        }
    }
}
