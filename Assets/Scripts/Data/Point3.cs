﻿using System.Xml.Serialization;

    [System.Serializable]
    public struct Point3 {

        [XmlAttribute("x")]
        public int x;

        [XmlAttribute("y")]
        public int y;

        [XmlAttribute("z")]
        public int z;

        public Point3(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Point3 scl(float scalar) {
            this.x = (int)(this.x * scalar);
            this.y = (int)(this.y * scalar);
            this.z = (int)(this.z * scalar);
            return this;
        }

        public static Point3 operator +(Point3 a, Point3 b) {
            a.x += b.x;
            a.y += b.y;
            a.z += b.z;
            return a;
        }

        public static Point3 operator -(Point3 a, Point3 b) {
            a.x -= b.x;
            a.y -= b.y;
            a.z -= b.z;
            return a;
        }

        public static Point3 operator *(Point3 a, Point3 b) {
            a.x *= b.x;
            a.y *= b.y;
            a.z *= b.z;
            return a;
        }

        public override string ToString() {
            return base.ToString() + "[" + x + ", " + y + ", " + z + "]";
        }
    }