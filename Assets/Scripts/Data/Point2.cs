﻿using System;
using System.Xml.Serialization;

    [System.Serializable]
    public struct Point2 {

        [XmlAttribute("x")]
        public int x;

        [XmlAttribute("y")]
        public int z;

        public Point2(int x, int y) {
            this.x = x;
            this.z = y;
        }

        public Point2 scl(float scalar) {
            this.x = (int)(this.x * scalar);
            this.z = (int)(this.z * scalar);
            return this;
        }

        public static Point2 operator +(Point2 a, Point2 b) {
            a.x += b.x;
            a.z += b.z;
            return a;
        }

        public static Point2 operator -(Point2 a, Point2 b) {
            a.x -= b.x;
            a.z -= b.z;
            return a;
        }

        public static Point2 operator *(Point2 a, Point2 b) {
            a.x *= b.x;
            a.z *= b.z;
            return a;
        }

        public override bool Equals(object obj) {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
                return false;

            Point2 p = (Point2)obj;
            return (x == p.x) && (z == p.z);
        }

        public override string ToString() {
            return base.ToString() + "[" + x + ", " + z + "]";
        }
    }