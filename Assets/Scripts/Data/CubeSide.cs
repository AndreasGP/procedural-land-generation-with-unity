﻿using System.Xml.Serialization;
using UnityEngine;

namespace ProceduralLandGeneration {

    public enum CubeSide {
        Ym = 0,
        YM = 1,
        Xm = 2,
        XM = 3,
        Zm = 4,
        ZM = 5
    }

    public static class CubeSideExtension {
        public static Point3 ToPoint(this CubeSide side) {
            switch (side) {
                case CubeSide.Xm:
                    return new Point3(-1, 0, 0);
                case CubeSide.XM:
                    return new Point3(1, 0, 0);
                case CubeSide.Zm:
                    return new Point3(0, 0, -1);
                case CubeSide.ZM:
                    return new Point3(0, 0, 1);
                case CubeSide.Ym:
                    return new Point3(0, -1, 0);
                case CubeSide.YM:
                    return new Point3(0, 1, 0);
            }
            return new Point3(0, 0, 0);
        }

        public static CubeSide Opposite(this CubeSide side) {
            switch (side) {
                case CubeSide.Xm:
                    return CubeSide.XM;
                case CubeSide.XM:
                    return CubeSide.Xm;
                case CubeSide.Zm:
                    return CubeSide.ZM;
                case CubeSide.ZM:
                    return CubeSide.Zm;
                case CubeSide.Ym:
                    return CubeSide.YM;
                case CubeSide.YM:
                    return CubeSide.Ym;
            }
            return CubeSide.Xm;
        }

        public static byte ToShapeMask(this CubeSide side) {
            switch (side) {
                case CubeSide.Xm:
                    return 0x99;
                case CubeSide.XM:
                    return 0x66;
                case CubeSide.Zm:
                    return 0x33;
                case CubeSide.ZM:
                    return 0xCC;
                case CubeSide.Ym:
                    return 0x0F;
                case CubeSide.YM:
                    return 0xF0;
            }
            return 0;
        }

        public static Color ToColor(this CubeSide side) {
            switch (side) {
                case CubeSide.Xm:
                    return new Color(0, 0.5f, 0.5f);
                case CubeSide.XM:
                    return new Color(1, 0.5f, 0.5f);
                case CubeSide.Zm:
                    return new Color(0.5f, 0.5f, 0);
                case CubeSide.ZM:
                    return new Color(0.5f, 0.5f, 1);
                case CubeSide.Ym:
                    return new Color(0.5f, 0, 0.5f);
                case CubeSide.YM:
                    return new Color(0.5f, 1, 0.5f);
            }
            return new Color(1f, 1f, 1f);
        }
    }
}