﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Pair<T1, T2> {

    public T1 x;
    public T2 y;

    public Pair(T1 x, T2 y) {
        this.x = x;
        this.y = y;
    }

    public override bool Equals(object obj) {
        if (obj is Pair<T1, T2>) {
            var other = (Pair<T1, T2>)obj;
            if (x.Equals(other.x) && y.Equals(other.y)) {
                return true;
            }
        }
        return false;
    }
}