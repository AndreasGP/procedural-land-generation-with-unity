﻿using ProceduralLandGeneration.Chunks;
using UnityEngine;

namespace ProceduralLandGeneration {

    public struct Biome {

        //Biome chunk coordinates
        public int biomeBlockX;
        public int biomeBlockZ;

        //Biome midpoint global coordinates
        public int globalBlockX;
        public int globalBlockZ;

        //The block corresponding to this biome
        public short block;

        //Amplification of this biome
        public float amplification;

        public float blendFactor;

        public float treeGenerationFrequency;

        private static System.Random random = new System.Random();

        public static int biomeXoffset = 0;
        public static int biomeYoffset = 0;

        //The frequency of the rainfall and temperature noise.
        //Smaller value = bigger biomes
        public static float biomeFrequency = 0.005f;

        public Biome(int biomeBlockX, int biomeBlockZ, int localBlockX, int localBlockZ) {
            this.biomeBlockX = biomeBlockX;
            this.biomeBlockZ = biomeBlockZ;

            
            globalBlockX = biomeBlockX * ChunkConstants.biomewidth + localBlockX; 
            globalBlockZ = biomeBlockZ * ChunkConstants.biomewidth + localBlockZ;

            //-700 and 500 offset generates the default terrain in a more scenic area than just (0, 0).
            var rainfall = Noise.Sum(Noise.valueMethods[1], new Vector3(globalBlockX + 9300 + biomeXoffset, globalBlockZ + biomeYoffset), biomeFrequency, 3, 2, 0.5f).value * 0.5f + 0.5f;
            var temperature = Noise.Sum(Noise.valueMethods[1], new Vector3(globalBlockX + biomeXoffset, globalBlockZ + 10500 + biomeYoffset), biomeFrequency, 3, 2, 0.5f).value * 0.5f + 0.5f;
            
            //Initialize some default values
            blendFactor = 1f;
            treeGenerationFrequency = 0;
            amplification = 1;
            block = Block.EMPTY;

            //Calculate the block in this biome.
            DetermineBlockType(rainfall, temperature);
        }

        private void DetermineBlockType(float rainfall, float temperature) {
            
            if(rainfall < 0.3f) {
                if(temperature > 0.7f) {
                    //Warm desert
                    block = Block.SAND;
                    amplification = 0.3f;
                    treeGenerationFrequency = 0.018f;
                    blendFactor = 1000f;
                } else if(temperature > 0.4f) {
                    //Cold desert
                    block = Block.SNOW;
                    amplification = 0.1f;
                    treeGenerationFrequency = 0.018f;
                } else {
                    //Tundra
                    block = Block.GRASS_DRY;
                    amplification = 0.8f;
                }
            } else {
                if(temperature < 0.5f) {
                    if(rainfall < 0.6f) {
                        //Grassland
                        block = Block.GRASS_NORMAL;
                        amplification = 0.4f;
                        treeGenerationFrequency = 0.5f;
                    } else {
                        //Sea
                        block = Block.WATER;
                        amplification = 0f;
                        blendFactor = 10000f;
                    }
                } else {
                    if(rainfall < 0.5f) {
                        //Savanna
                        block = Block.GRASS_HOT;
                        amplification = 0.4f;
                        treeGenerationFrequency = 0.05f;
                    } else {
                        //Forest
                        block = Block.GRASS_FOREST;
                        amplification = 0.5f;
                        treeGenerationFrequency = 0.19f;
                    }
                }

            }

        }
        
        /// <summary>
        /// Returns the distance to the given global point.
        /// </summary>
        public float GetDistanceSquare(int globalX, int globalZ) {
            return (globalBlockX - globalX) * (globalBlockX - globalX) + (globalBlockZ - globalZ) * (globalBlockZ - globalZ);
        }

        public override string ToString() {
            return "B: " + block + "| biomeBlock: " + biomeBlockX + " " + biomeBlockZ;
        }

    }
    
}