﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

namespace ProceduralLandGeneration.Chunks {

    public class UIManager : Singleton<UIManager> {

        #region serialized fields
        [SerializeField]
        private Toggle autoGenerateLandToggle;

        [SerializeField]
        private Slider renderRadiusSlider;

        [SerializeField]
        private InputField biomeXoffsetInputField;

        [SerializeField]
        private InputField biomeYoffsetInputField;

        [SerializeField]
        private InputField biomeChunkWidthInputField;

        [SerializeField]
        private InputField biomeFrequencyInputField;

        [SerializeField]
        private Button regenerateChunksButton;

        [SerializeField]
        private GameObject uiPanel;
        #endregion

        void Start() {
            autoGenerateLandToggle.onValueChanged.AddListener(OnAutoGenerateLandToggled);
            renderRadiusSlider.onValueChanged.AddListener(OnRenderRadiusChanged);
            biomeXoffsetInputField.onEndEdit.AddListener(OnBiomeXOffsetChanged);
            biomeYoffsetInputField.onEndEdit.AddListener(OnBiomeYOffsetChanged);
            biomeChunkWidthInputField.onEndEdit.AddListener(OnBiomeChunkWidthChanged);
            biomeFrequencyInputField.onEndEdit.AddListener(OnBiomeFrequencyChanged);
            regenerateChunksButton.onClick.AddListener(OnRegenerateChunksClicked);
        }

        void Update() {
            if(Input.GetKeyDown(KeyCode.F)) {
                uiPanel.gameObject.SetActive(!uiPanel.gameObject.activeSelf);
            }
        }

        private void OnAutoGenerateLandToggled(bool newValue) {
            ChunkManager.ins.autoGenerate = newValue;
        }

        private void OnRenderRadiusChanged(float newRadius) {
            ChunkManager.ins.generationRadius = (int)newRadius;
        }

        private void OnBiomeXOffsetChanged(string newOffset) {
            var xOffset = 0;
            if(int.TryParse(newOffset, out xOffset)) {
                Biome.biomeXoffset = xOffset;
            }
        }

        private void OnBiomeYOffsetChanged(string newOffset) {
            var yOffset = 0;
            if (int.TryParse(newOffset, out yOffset)) {
                Biome.biomeYoffset = yOffset;
            }
        }

        private void OnBiomeChunkWidthChanged(string newWidth) {
            var width = 0;
            if (int.TryParse(newWidth, out width)) {
                ChunkConstants.biomewidth = width;
            }
        }

        private void OnBiomeFrequencyChanged(string newFrequency) {
            var frequency = 0f;
            if (float.TryParse(newFrequency, out frequency)) {
                Biome.biomeFrequency = frequency;
            }
        }

        private void OnRegenerateChunksClicked() {
            ChunkManager.ins.UnloadAll();
        }

    }
}
