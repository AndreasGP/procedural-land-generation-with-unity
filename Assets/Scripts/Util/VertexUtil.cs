﻿using ProceduralLandGeneration.Chunks;
using System.Collections.Generic;
using UnityEngine;

namespace ProceduralLandGeneration.Util {

    public static class VertexUtil {

        /** Returns the vector3 position with values 0 or Chunk.scale based on corner 
        * For example corner 7 will return Vec3(0, Chunk.scale, Chunk.scale) */
        public static Vector3 ToVertexPosition(int vertex) {
            Vector3 position = new Vector3();
            switch (vertex) {
                case 0:
                    break;
                case 1:
                    position.x = ChunkConstants.blockwidth;
                    break;
                case 2:
                    position.x = ChunkConstants.blockwidth;
                    position.z = ChunkConstants.blockwidth;
                    break;
                case 3:
                    position.z = ChunkConstants.blockwidth;
                    break;
                case 4:
                    position.y = ChunkConstants.blockheight;
                    break;
                case 5:
                    position.x = ChunkConstants.blockwidth;
                    position.y = ChunkConstants.blockheight;
                    break;
                case 6:
                    position.x = ChunkConstants.blockwidth;
                    position.y = ChunkConstants.blockheight;
                    position.z = ChunkConstants.blockwidth;
                    break;
                case 7:
                    position.y = ChunkConstants.blockheight;
                    position.z = ChunkConstants.blockwidth;
                    break;
            }
            return position;
        }

    }
}