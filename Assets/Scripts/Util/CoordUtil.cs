﻿using ProceduralLandGeneration.Chunks;
using UnityEngine;

namespace ProceduralLandGeneration.Util {

    public static class CoordUtil {

        public static Point3 GlobalPositionToGlobalBlock(Vector3 pos) {
            return GlobalPositionToGlobalBlock(pos.x, pos.y, pos.z);
        }

        public static Point3 GlobalPositionToGlobalBlock(float x, float y, float z) {
            int _x = x >= 0 ? (int)(x / ChunkConstants.blockwidth) : (int)(x / ChunkConstants.blockwidth) - 1;
            int _y = (int)(y / ChunkConstants.blockheight);
            int _z = z >= 0 ? (int)(z / ChunkConstants.blockwidth) : (int)(z / ChunkConstants.blockwidth) - 1;
            return new Point3(_x, _y, _z);
        }

        public static Point2 GlobalBlockToBiomeBlock(Point3 globalBlock) {
            return GlobalBlockToBiomeBlock(globalBlock.x, globalBlock.z);
        }

        public static Point2 GlobalBlockToBiomeBlock(int x, int z) {
            int _x = (x >= 0 ? x / ChunkConstants.biomewidth : -1 + (x + 1) / ChunkConstants.biomewidth);
            int _z = (z >= 0 ? z / ChunkConstants.biomewidth : -1 + (z + 1) / ChunkConstants.biomewidth);
            return new Point2(_x, _z);
        }

        public static Point3 GlobalBlockToLocalBlock(Point3 globalBlock) {
            return GlobalBlockToLocalBlock(globalBlock.x, globalBlock.y, globalBlock.z);
        }

        public static Point3 GlobalBlockToLocalBlock(int x, int y, int z) {
            int _x = (x >= 0 ? x % ChunkConstants.chunkwidth : ChunkConstants.chunkwidth - 1 - (-x - 1) % ChunkConstants.chunkwidth);
            int _z = (z >= 0 ? z % ChunkConstants.chunkwidth : ChunkConstants.chunkwidth - 1 - (-z - 1) % ChunkConstants.chunkwidth);
            return new Point3(_x, y, _z);
        }

        public static Point2 GlobalBlockToChunk(Point3 globalBlock) {
            return GlobalBlockToChunk(globalBlock.x, globalBlock.y, globalBlock.z);
        }

        public static Point2 GlobalBlockToChunk(int x, int y, int z) {
            int _x = x >= 0 ? x / ChunkConstants.chunkwidth : (x - ChunkConstants.chunkwidth + 1) / ChunkConstants.chunkwidth;
            int _z = z >= 0 ? z / ChunkConstants.chunkwidth : (z - ChunkConstants.chunkwidth + 1) / ChunkConstants.chunkwidth;
            return new Point2(_x, _z);
        }


        public static bool IsOnLocalChunk(Point3 localBlock) {
            return IsOnLocalChunk(localBlock.x, localBlock.y, localBlock.z);
        }

        public static bool IsOnLocalChunk(int x, int y, int z) {
            return x >= 0 && y >= 0 && z >= 0 && x < ChunkConstants.chunkwidth && y < ChunkConstants.chunkheight && z < ChunkConstants.chunkwidth;
        }

        public static Point3 LocalBlockToGlobalBlock(Point2 chunkPos, Point3 localBlock) {
            return LocalBlockToGlobalBlock(chunkPos.x, chunkPos.z, localBlock.x, localBlock.y, localBlock.z);
        }

        public static Point3 LocalBlockToGlobalBlock(int chunkX, int chunkZ, int x, int y, int z) {
            int _x = chunkX * ChunkConstants.chunkwidth + x;
            int _y = y;
            int _z = chunkZ * ChunkConstants.chunkwidth + z;
            return new Point3(_x, _y, _z);
        }


        /** Translates local 3D coordinates as x, y, z into 1D coordinate i.
        * For example x = 2, y = 3; z = 1; and chunkwidth 4 would translate to 
        * x * 16 + z * 4 + y = 32 + 4 + 3 = 39.
        * Warning: This does not ensure coordinates are local. Make sure of that yourself.
        * @param x local x coordinate
        * @param y local y coordinate
        * @param z local z coordinate
        * @return Local i coordinate
        */
        public static int xyzToI(int x, int y, int z) {
            return y * ChunkConstants.chunkarea + z * ChunkConstants.chunkwidth + x;
        }

        /** Translates local 1D coordinate i to local 3D coordinates as x, y, z.
         * For example 39 and chunkwidth 4 would be translated into x = 2, y = 3, z = 1.
         * (x = 39 / (4*4) = 39 / 16 = 2, y = 39 % 4 = 3, z = (39 - x * 4 * 4) / 4 = 7 / 4 = 1)
         * @param i 1D coordinate
         * @return 3D coordinate as (x, y, z) Point3
         */
        public static Point3 iToXYZ(int i) {
            //Divides i by CHUNKAREA, see http://stackoverflow.com/questions/18560844/does-java-optimize-division-by-powers-of-two-to-bitshifting
            int y = i >> (ChunkConstants.chunkwidthbit * 2);
            //Takes i % ChunkConstants.chunkwidth in an optimized way.
            int x = modulo(i, ChunkConstants.chunkwidth);
            //Divides first half by CHUNKWIDTH
            int z = (i - y * ChunkConstants.chunkarea) >> (ChunkConstants.chunkwidthbit);
            return new Point3(x, y, z);
        }

        /** Takes x modulo y if y is a power of two. Warning: No check for y is made.
         * Read more: http://dhruba.name/2011/07/12/performance-pattern-modulo-and-powers-of-two/
         * @return x % y, if y is power of two.
         */
        private static int modulo(int x, int y) {
            return x & (y - 1);
        }


    }
}
