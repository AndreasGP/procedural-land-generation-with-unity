﻿using System.Collections.Generic;
using UnityEngine;

namespace ProceduralLandGeneration.Util {

    public static class MeshTable {

        /// Dictionary with Sides for keys, used for faces which are full squares
        /// int[] value is a set of 4 corners to be used to generate vertex positions.
        public static Dictionary<CubeSide, int[]> squareTable = new Dictionary<CubeSide, int[]>()
            {
            { CubeSide.Xm, new int[] { 3, 0, 7, 4 } },
            { CubeSide.XM, new int[] { 1, 2, 5, 6 } },
            { CubeSide.Zm, new int[] { 0, 1, 4, 5 } },
            { CubeSide.ZM, new int[] { 2, 3, 6, 7 } },
            { CubeSide.Ym, new int[] { 2, 1, 3, 0 } },
            { CubeSide.YM, new int[] { 7, 4, 6, 5 } },
        };

        /// <summary>
        /// A list of normals. First array contains id 0-5 of the corresponding CubeSide.
        /// The array inside it contains 4 normals for that side
        /// </summary>
        public static Vector3[] normalTable = new Vector3[] {
            Vector3.down,
            Vector3.up,
            Vector3.left,
            Vector3.right,
            Vector3.back,
            Vector3.forward
        };

    }
}